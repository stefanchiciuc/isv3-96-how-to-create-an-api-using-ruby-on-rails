# frozen_string_literal: true

Friend.create([
                { name: 'Nemwel Boniface', phone: 754_135_545, twitter: '@nemwel_bonie', email: 'nemwelboniface@outlook.com',
                  location: 'Nairobi, Kenya' },
                { name: 'John Black', phone: 754_135_545, twitter: '@john_black', email: 'johnblack@gmail.com',
                  location: 'Leova, Moldova' },
                { name: 'Mary Jane', phone: 754_135_545, twitter: '@mary_jane', email: 'marymary@gmail.com',
                  location: 'New York, USA' }
              ])

Message.create([
                 {
                   sender_id: 1, # Replace with a valid sender_id from your Friend records
                   receiver_id: 2, # Replace with a valid receiver_id from your Friend records
                   message_content: "Hey, how's it going?",
                   read: false
                 },
                 {
                   sender_id: 2, # Replace with a valid sender_id from your Friend records
                   receiver_id: 1, # Replace with a valid receiver_id from your Friend records
                   message_content: "I'm good! How about you?",
                   read: true
                 }
                 # Add more messages as needed
               ])
