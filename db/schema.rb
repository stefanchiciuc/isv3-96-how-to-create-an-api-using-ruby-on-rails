# frozen_string_literal: true

# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 20_230_904_132_217) do
  # These are extensions that must be enabled in order to support this database
  enable_extension 'plpgsql'

  create_table 'friends', force: :cascade do |t|
    t.string 'name'
    t.integer 'phone'
    t.string 'twitter'
    t.string 'email'
    t.string 'location'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
  end

  create_table 'messages', force: :cascade do |t|
    t.bigint 'sender_id', null: false
    t.bigint 'receiver_id', null: false
    t.text 'message_content'
    t.boolean 'read'
    t.datetime 'created_at', null: false
    t.datetime 'updated_at', null: false
    t.index ['receiver_id'], name: 'index_messages_on_receiver_id'
    t.index ['sender_id'], name: 'index_messages_on_sender_id'
  end

  add_foreign_key 'messages', 'friends', column: 'receiver_id'
  add_foreign_key 'messages', 'friends', column: 'sender_id'
end
