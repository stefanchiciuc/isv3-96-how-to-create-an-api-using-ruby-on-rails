# frozen_string_literal: true

class CreateMessages < ActiveRecord::Migration[7.0]
  def change
    create_table :messages do |t|
      t.references :sender, null: false, foreign_key: { to_table: :friends }
      t.references :receiver, null: false, foreign_key: { to_table: :friends }
      t.text :message_content
      t.boolean :read

      t.timestamps
    end
  end
end
