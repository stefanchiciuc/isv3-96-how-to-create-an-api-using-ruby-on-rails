# frozen_string_literal: true

class Message < ApplicationRecord
  belongs_to :sender, class_name: 'Friend', foreign_key: 'sender_id'
  belongs_to :receiver, class_name: 'Friend', foreign_key: 'receiver_id'
  validates :message_content, length: { maximum: 50 }
  validates :read, inclusion: { in: [true, false] }
end
