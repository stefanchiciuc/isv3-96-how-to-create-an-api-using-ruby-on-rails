# frozen_string_literal: true

class Friend < ApplicationRecord
  validates :name, length: { minimum: 2, maximum: 20 }
  validates :location, length: { minimum: 2, maximum: 20 }
  validates :email, length: { minimum: 2, maximum: 30 }
  validates :twitter, length: { minimum: 2, maximum: 20 }
  validates :phone, length: { is: 9 }, numericality: true
end
