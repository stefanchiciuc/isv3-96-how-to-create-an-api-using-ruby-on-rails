# frozen_string_literal: true

module Api
  module V1
    class MessagesController < ApplicationController
      before_action :set_message, only: %i[show update destroy]

      # GET /api/v1/messages
      def index
        messages = Message.all

        if messages
          render json: { status: 'SUCCESS', message: 'Fetched all messages successfully', data: messages }, status: :ok
        else
          render json: messages.errors, status: :bad_request
        end
      end

      # POST /api/v1/messages
      def create
        message = Message.new(message_params)

        if message.save
          render json: { status: 'SUCCESS', message: 'Message was created successfully!', data: message },
                 status: :created
        else
          render json: message.errors, status: :unprocessable_entity
        end
      end

      # GET /api/v1/messages/:id
      def show
        if @message
          render json: { data: @message }, state: :ok
        else
          render json: { message: 'Message could not be found' }, status: :bad_request
        end
      end

      # DELETE /api/v1/messages/:id
      def destroy
        if @message.destroy!
          render json: { message: 'Message was deleted successfully' }, status: :ok
        else
          render json: { message: 'Message does not exist' }, status: :bad_request
        end
      end

      # PUT /api/v1/messages/:id
      def update
        if @message.update!(message_params)
          render json: { message: 'Message was updated successfully', data: @message }, status: :ok
        else
          render json: { message: 'Message cannot be updated' }, status: :unprocessable_entity
        end
      end

      def messages_sent_by_user
        sender_id = params[:sender_id]
        messages = Message.where(sender_id:)

        if messages
          render json: { status: 'SUCCESS', message: 'Fetched messages sent by user successfully', data: messages },
                 status: :ok
        else
          render json: { message: 'No messages found for the specified user' }, status: :not_found
        end
      end

      def messages_received_by_user
        receiver_id = params[:receiver_id]
        messages = Message.where(receiver_id:)

        if messages
          render json: { status: 'SUCCESS', message: 'Fetched messages received by user successfully', data: messages },
                 status: :ok
        else
          render json: { message: 'No messages found for the specified user' }, status: :not_found
        end
      end

      def messages_true
        messages = filter_messages_by_read(true)
        render_messages(messages)
      end

      # GET /api/v1/messages/false
      def messages_false
        messages = filter_messages_by_read(false)
        render_messages(messages)
      end

      private

      def set_message
        @message = Message.find(params[:id])
      end

      def message_params
        params.require(:message).permit(:sender_id, :receiver_id, :message_content, :timestamp, :read)
      end

      def filter_messages_by_read(read_status)
        Message.where(read: read_status)
      end

      def render_messages(messages)
        if messages
          render json: { status: 'SUCCESS', message: 'Fetched messages successfully', data: messages }, status: :ok
        else
          render json: { message: 'No messages found' }, status: :not_found
        end
      end
    end
  end
end
