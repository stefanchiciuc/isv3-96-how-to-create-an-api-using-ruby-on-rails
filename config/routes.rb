# frozen_string_literal: true

Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :friends
      resources :messages do
        collection do
          get 'sent_by_user/:sender_id', action: 'messages_sent_by_user'
          get 'received_by_user/:receiver_id', action: 'messages_received_by_user'
          get 'true', action: 'messages_true'
          get 'false', action: 'messages_false'
        end
      end
    end
  end
end
