
# Creating a Rails API

This API is built with Ruby on Rails and is using PostgreSQL database,
 the app has 2 tables: Friends and Messages. The Friends table has a many to many relationship with the Messages table. Each Message has a sender and a receiver, both of which are id from friends table.

## Getting Started
Follow these steps to set up and run the API:

1. Clone this repository to your local machine.

2. In the `.env` file put all your environment variables from PostgreSQL.

3. Run following commands to build and run the API:
    
```bash
docker-compose build
docker-compose up -d
```

4. Run the following commands to create, migrate and seed the database:

```bash
docker-compose exec app rails db:create
docker-compose exec app rails db:migrate
docker-compose exec app rails db:seed
```
5. Open Postman and copy the `ISV3-96.postman_collection.json` file into it to verify the following endpoints:
---
- GET /api/v1/friends - returns all friends
- GET /api/v1/friends/:id - returns a friend with the given id
- POST /api/v1/friends - creates a new friend
- PUT /api/v1/friends/:id - updates a friend with the given id
- DELETE /api/v1/friends/:id - deletes a friend with the given id
---
- GET /api/v1/messages - returns all messages
- GET /api/v1/messages/:id - returns a message with the given id
- POST /api/v1/messages - creates a new message
- PUT /api/v1/messages/:id - updates a message with the given id
- DELETE /api/v1/messages/:id - deletes a message with the given id
---
- GET /api/v1/messages/sent_by_user/:id - returns all messages sent by user with a specific ID
- GET /api/v1/messages/received_by_user/:id - returns all messages received by user with a specific ID
- GET /api/v1/messages/true - returns all messages that have been read
- GET /api/v1/messages/false - returns all messages that have not been read